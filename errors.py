class Error(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class NotFoundError(Error):
    pass


class ValidationError(Error):
    pass