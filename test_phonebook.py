import unittest

from errors import NotFoundError, ValidationError
from phonebook import PhoneBook


class TestPhonebook(unittest.TestCase):

    def setUp(self):
        self.pb = PhoneBook('test_db.json')

    def test_loading_data(self):
        data = self.pb.read()
        self.assertEqual(
            data,
            [{'id': 'd15857ba-39d4-44fe-8c40-80a0f37807c9', 'name': 'John', 'surname': 'Smith', 'phone': '123456789'}]
        )

    def test_get_contact_by_id(self):
        one = self.pb.get_contact_by_id('d15857ba-39d4-44fe-8c40-80a0f37807c9')
        self.assertEqual(
            {'id': 'd15857ba-39d4-44fe-8c40-80a0f37807c9', 'name': 'John', 'surname': 'Smith', 'phone': '123456789'},
            one
        )

    def test_if_no_contact_raise_not_found(self):
        self.assertRaises(NotFoundError, self.pb.get_contact_by_id, '014cc93a-302b-4c4e-81d6-9a669cbf616d')

    def test_add_contact(self):
        new_rec = {
            "id": "98032fdf-b83c-4ba2-9c2d-6413a7644b0c",
            "name": "Test",
            "surname": "Surtest",
            "phone": "123456789"
        }
        self.pb.save_contact(new_rec.get("id"), new_rec.get("name"), new_rec.get("surname"), new_rec.get("phone"))
        rec = self.pb.get_contact_by_id(new_rec.get("id"))
        self.assertEqual(new_rec, rec)
        self.assertEqual(2, self.pb.read())

    def test_validation_when_adding_contact(self):
        self.assertRaises(ValidationError,
                          self.pb.save_contact,
                          "98032fdf-b83c-4ba2-9c2d-6413a7644b0c",
                          "Test",
                          "Surtest",
                          ""
                          )

    def test_remove_contact_by_id(self):
        self.pb.remove_contact_by_id("98032fdf-b83c-4ba2-9c2d-6413a7644b0c")
        self.assertEqual(1, len(self.pb.read()))

    def test_when_contact_to_be_removed_not_found(self):
        self.assertRaises(NotFoundError, self.pb.remove_contact_by_id, '014cc93a-302b-4c4e-81d6-9a669cbf616d')

if __name__ == '__main__':
    unittest.main()
