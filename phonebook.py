# Prosze napisac prosta baze danych do przechowywania numerow telefonow jakiejs grupy
# ludzi.
# Dane moga byc przechowywane w postaci slownika lub dowolnej innej (np. lista par
# nazwisko-numer).
# 
# Operacje, ktore powinien umozliwiac program:
# - dodanie rekordu,
# - usuniecie rekordu,
# - wyswietlenie rekordu (wszystkich rekordow),
# - zapis i odczyt do pliku.
import json
import sys
import uuid

from errors import NotFoundError, ValidationError


class PhoneBook:
    valid_commands = {
        "1": "wyświetl wszystkie wpisy",
        "2": "wyswietl wpis",
        "3": "dodaj nowy wpis",
        "4": "usun wpis",
        "?": "wyświetl opcje",
        "x": "wyjdz"
    }

    def __init__(self, db='db.json'):
        self.file = db

    def display_init(self):
        print("Książka telefoniczna v.0.0.1")
        print("")

    def display_info(self):
        print("")
        print("Wybierz opcję:")
        for k, v in self.valid_commands.items():
            print(f"[{k}] {v}")
        self.read_input()

    def run_command(self, cmd):
        if cmd not in self.valid_commands:
            print("Wybrano nieistniejącą opcję, spróbuj ponownie.")
            self.read_input()
        else:
            if cmd == "1":
                self.display_all()
            elif cmd == "2":
                self.show_one(input("Podaj ID wpisu do wyswietlenia: "))
            elif cmd == "3":
                self.add_one()
            elif cmd == "4":
                self.del_one(input("Podaj ID wpisu do usuniecia: "))
            elif cmd == "x":
                print("OK, THX, BYE")
                sys.exit()
            else:
                print("not implemented yet :P")
            self.display_info()

    def read_input(self):
        return self.run_command(input("Podaj opcję: "))

    def read(self):
        with open(self.file, "r") as db:
            book = json.load(db)
        db.close()
        return book

    def save(self, data):
        json_data = json.dumps(data)
        with open(self.file, "w") as db:
            db.write(json_data)
        db.close()

    @staticmethod
    def display(rec):
        print("")
        print("{:>10}| {}".format("ID", rec.get("id")))
        print("{:-<45}".format(""))
        print("{:>10}| {}".format("Imie", rec.get("name")))
        print("{:>10}| {}".format("Nazwisko", rec.get("surname")))
        print("{:>10}| {}".format("Telefon", rec.get("phone")))
        print("")

    def display_all(self):
        book = self.read()
        print("")
        print("{:<40}| {:<6}| {:<20}".format("ID", "Imie", "Nazwisko"))
        print("{:-<96}".format(""))
        for rec in book:
            print(
                "{:<40}| {:<6}| {:<20}".format(
                    rec.get("id"),
                    rec.get("name")[:1] + ".",
                    rec.get("surname")
                )
            )
        total = len(book)
        print(f"Lacznie wpisow: {total}")

    def add_one(self):
        rec_id = str(uuid.uuid4())
        name = input("Imię: ")
        surname = input("Nazwisko: ")
        phone = input("Telefon: ")
        try:
            self.save_contact(rec_id, name, surname, phone)
        except ValidationError as e:
            print("Wystapil blad podczas zapisu kontaktu: "+e.message)
            print("Sprobuj jeszcze raz.")
            self.add_one()

    def del_one(self, rec_id):
        if "t" != input("Czy na pewno? [t/N]: "):
            return

        try:
            self.remove_contact_by_id(rec_id)
        except NotFoundError as e:
            print(e.message)
        print(f"Usunieto rekord o ID: {rec_id}")

    def show_one(self, rec_id):
        try:
            self.display(self.get_contact_by_id(rec_id))
        except NotFoundError as e:
            print(e.message)

    def get_contact_by_id(self, rec_id):
        book = self.read()
        for rec in book:
            if rec.get("id") == rec_id:
                return rec
        raise NotFoundError(f"Nie znaleziono rokordu z ID: {rec_id}")

    def save_contact(self, rec_id, name, surname, phone):
        rec = {"id": rec_id, "name": name, "surname": surname, "phone": phone}
        for k, v in rec.items():
            if v == '':
                raise ValidationError(f"Podano bledny \"{k}\"")
        book = self.read()
        book.append(rec)
        self.save(book)

    def remove_contact_by_id(self, rec_id):
        book = self.read()
        for rec in book:
            if rec.get("id") == rec_id:
                book.remove(rec)
                self.save(book)
                return
        raise NotFoundError(f"Nie usunieto rekordu, poniewaz rokord z ID: {rec_id} nie istnieje")


